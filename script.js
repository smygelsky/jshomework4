
function createNewUser() {
    fName = prompt('Enter first name');
    lName = prompt('Enter last name');

    newUser = {
        _firstName: fName,
        _lastName: lName,

        setFirstName(newValue){
                 this._firstName = newValue;
        },

        setLastName(newValue){
                this._lastName = newValue;
        },

        getLogin: function(){
            return (this._firstName.substring(0,1) + this._lastName).toLowerCase();
        },

        get FirstName(){
            return this._firstName;
        },


        get LastName(){
            return this._lastName;
        }

    };
    return newUser
}


let user =  createNewUser();

console.log("Login: "+user.getLogin());
